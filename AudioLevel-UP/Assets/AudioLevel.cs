﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioLevel : MonoBehaviour {
    public int micDeviceId = 0;

    // nimmt die daten vom mic auf
    AudioSource audiorecorder;

    // zum darstellen des mic volumes (sonst keine funktion)
    [Range(0,1000)]
    public float volume = 0;

    void Awake() {
        audiorecorder = GetComponent<AudioSource>();
        audiorecorder.loop = true;
        audiorecorder.clip = Microphone.Start(
            Microphone.devices[micDeviceId],
            true,
            1,
            AudioSettings.outputSampleRate);
        audiorecorder.Play();
    }

    // this handles the microphone data, it sends the data and deletes any further audio output
    void OnAudioFilterRead(float[] data, int channels) {
        float minVal = float.MaxValue;
        float maxVal = float.MinValue;
        float curVal = 0;
        for (int i = 0; i < data.Length; i++) {
            curVal = data[i];
            if (curVal < minVal)
                minVal = curVal;
            if (curVal > maxVal)
                maxVal = curVal;
            
            // clear array so we dont output any sound, IMPORTANT TO PREVENT AUDIO FEEDBACK LOOP!!!
            data[i] = 0;
        }

        volume = (maxVal-minVal) * 1000;
    }
}